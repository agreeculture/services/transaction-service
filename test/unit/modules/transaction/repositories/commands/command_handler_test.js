

const commandHandler = require('../../../../../../bin/modules/transaction/repositories/commands/command_handler');
const domain = require('../../../../../../bin/modules/transaction/repositories/commands/domain');
const validator = require('../../../../../../bin/modules/transaction/utils/validator');
const wrapper = require('../../../../../../bin/helpers/utils/wrapper');
const EventEmitter = require('events').EventEmitter;
const sinon = require('sinon');
const assert = require('assert');

describe('postOneTransaction', () => {

  let queryResult = {
    err: null,
    data: {
      id: '1',
      offerId: '1',
      status: 'test',
      termin: '10000'
    },
    message: 'Your Request Has Been Processed',
    code: 200
  };

  let payload = {
      id: '1',
      offerId: '1',
      status: 'test',
      termin: '10000'
  };

  it('Should return product object', async () => {

    sinon.stub(domain.prototype, 'addNewTransaction').returns(queryResult);
    sinon.stub(validator, 'ifExistTransaction').returns(queryResult);

    const rs = await commandHandler.postOneTransaction(payload);

    assert.equal(rs.err, null);
    assert.equal(rs.code, 200);
    assert.equal(rs.data.status, 'test');

    domain.prototype.addNewTransaction.restore();
    validator.ifExistTransaction.restore();

  });

  // it('Should return error event store mockup object', async () => {

  //     let obj = {
  //         err: new Error('whoops!'),
  //         data: null,
  //         message: 'Your Request Can not Processed',
  //         code: 500
  //     };

  //     const spy = sinon.spy();
  //     const emitter = new EventEmitter;

  //     emitter.on('error', spy);
  //     emitter.emit('error')
  //     sinon.stub(domain.prototype, 'addNewMockup').returns(obj);
  //     sinon.stub(domain.prototype, 'publishNewMockup').returns(obj);
  //     sinon.stub(validator, 'ifExistMockup').returns(queryResult);

  //     const rs = await commandHandler.postOneMockup(payload);

  //     // console.log
  //     // assert.equal(rs.err, null);
  //     assert.equal(rs.code, 500);

  //     sinon.assert.calledOnce(spy);

  //     domain.prototype.addNewMockup.restore();
  //     domain.prototype.publishNewMockup.restore();
  //     validator.ifExistMockup.restore();

  // });

});
