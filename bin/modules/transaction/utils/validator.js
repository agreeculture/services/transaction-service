'use strict';

const validate = require("validate.js");
const wrapper = require('../../../helpers/utils/wrapper');
const Mongo = require('../../../helpers/databases/mongodb/db');
const MySQL = require('../../../helpers/databases/mysql/db');
const config = require('../../../infra/configs/global_config');

const validateConstraints = async (values,constraints) => {
    if(validate(values,constraints)){
        return wrapper.error('Bad Request',validate(values,constraints),400);
    }else{
        return wrapper.data(true);
    }
}

const isValidParamGetOneTransaction = async (payload) => {
    let constraints = {};
    let values = {};
    constraints[payload.status] = {length: {minimum: 1}};
    constraints[payload.termin] = {length: {minimum: 1}};
    values[payload.status] = payload.status;
    values[payload.termin] = payload.termin;

    return await validateConstraints(values,constraints);
}

const isValidParamGetAllTransactions = async (payload) => {
    let constraints = {};
    let values = {};
    constraints[payload.status] = {length: {minimum: 1}};
    constraints[payload.termin] = {length: {minimum: 1}};
    values[payload.status] = payload.status;
    values[payload.termin] = payload.termin;
    return await validateConstraints(values,constraints);
}

const isValidParamPostOneTransaction = async (payload) => {
    let constraints = {};
    let values = {};
    constraints[payload.status] = {length: {minimum: 1}};
    constraints[payload.termin] = {length: {minimum: 1}};
    values[payload.status] = payload.status;
    values[payload.termin] = payload.termin;
    return await validateConstraints(values,constraints);
}

const ifExistTransaction = async (payload) => {
    const id = payload.id;
    const db = new MySQL(config.getDevelopmentDBMySQL());
    const query = `SELECT * FROM transaction WHERE id='${id}';`;
    const result = await db.findOne(query);
    return result;
}

module.exports = {
    isValidParamGetOneTransaction: isValidParamGetOneTransaction,
    isValidParamGetAllTransactions: isValidParamGetAllTransactions,
    isValidParamPostOneTransaction: isValidParamPostOneTransaction,
    ifExistTransaction: ifExistTransaction,
}