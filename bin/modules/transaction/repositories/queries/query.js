'use strict';

const MySQL = require('../../../../helpers/databases/mysql/db');
const config = require('../../../../infra/configs/global_config');
const ObjectId = require('mongodb').ObjectId;

const findOneTransaction = async (parameter) => {
    const db = new MySQL(config.getDevelopmentDBMySQL());
    const query = `SELECT * FROM transaction WHERE id='${parameter.id}';`;
    const recordset = await db.findOne(query);
    return recordset;
}

const findAllTransactions = async () => {
    const db = new MySQL(config.getDevelopmentDBMySQL());
    const query = `SELECT * FROM transaction;`;
    const recordset = await db.findMany(query);
    return recordset;
}


module.exports = {
    findOneTransaction: findOneTransaction,
    findAllTransactions: findAllTransactions
}