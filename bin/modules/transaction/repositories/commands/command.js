'use strict';

const MySQL = require('../../../../helpers/databases/mariadb/db');
const wrapper = require('../../../../helpers/utils/wrapper');
const config = require('../../../../infra/configs/global_config');

const insertOneTransaction = async (params) => {
    const db = new MySQL(config.getDevelopmentDBMySQL());
    const queryInsert = `INSERT INTO transaction SET ?`;
    const result = await db.query(queryInsert, params);
    //console.log(result);
    return result;
}

const updateOneTransaction = async (params, updateQuery) => {
    const id = params.id;
    const db = new MySQL(config.getDevelopmentDBMySQL());
    const queryUpdate = `UPDATE transaction WHERE id= ?`;
    const result = await db.command(queryUpdate, id);
    return result;
}

const deleteOneTransaction = async (params) => {
    const id = params.id;
    const db = new MySQL(config.getDevelopmentDBMySQL());
    const query = `DELETE FROM transaction WHERE id='${id}';`;
    const result = await db.command(query);
    return result;
}

module.exports = {
    insertOneTransaction: insertOneTransaction,
    updateOneTransaction: updateOneTransaction,
    deleteOneTransaction: deleteOneTransaction
}