'use strict';

const generalTransaction = () => {
    const model = {
        id:``,
        offerId:``,
        status:``,
        termin:``,
        createdAt: new Date(),
        updatedAt: null
    }
    return model;
}

module.exports = {
    generalTransaction: generalTransaction
}
