// const nconf   = require('nconf');
// const apm = require('elastic-apm-node').start({
//   serviceName: 'product-service',

// });

const restify = require('restify');

//const app = restify.createServer();
// const serveStatic = require('serve-static-restify');
const project = require('../../package.json');
const basicAuth = require('../auth/basic_auth_helper');
// const jwtAuth = require('../auth/jwt_auth_helper');
const wrapper = require('../helpers/utils/wrapper');
// const sentryLog = require('../helpers/components/sentry/sentry_log');
// const logger = require('../helpers/utils/logger');
const corsMiddleware = require('restify-cors-middleware');
const transactionHandler = require('../modules/transaction/handlers/api_handler');

// app.get('/hello/:name', function (req, res, next) {
//   res.send('hello ' + req.params.name)
//   next()
// })

//app.listen(3000)

let crossOrigin = (req,res,next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  if ('OPTIONS' == req.method) {
    res.send(200);
  }
  return next();
};

const cors = corsMiddleware({
  preflightMaxAge: 5, //Optional
  origins: ['*'],
  allowHeaders: ['Origin, X-Requested-With, Content-Type, Accept, OPTIONS'],
  exposeHeaders: ['OPTIONS']
});

let AppServer = function(){
  this.server = restify.createServer({
    name: project.name + '-server',
    version: project.version
  });

  this.server.serverKey = '';
  this.server.pre(cors.preflight);
  this.server.use(cors.actual);
  this.server.use(restify.plugins.acceptParser(this.server.acceptable));
  this.server.use(restify.plugins.queryParser());
  this.server.use(restify.plugins.bodyParser());
  this.server.use(restify.plugins.authorizationParser());

  //required for basic auth
  this.server.use(basicAuth.init());
  this.server.use(crossOrigin);

  //anonymous can access the end point, place code bellow
  this.server.get('/', (req, res, next) => {
    wrapper.response(res,'success',wrapper.data('Index'),'This service is running properly');
  });

  //Transaction
  this.server.post('/api/v1/transaction/', transactionHandler.postOneTransaction);
  this.server.get('/api/v1/transaction/', transactionHandler.getAllTransactions);
  this.server.get('/api/v1/transaction/:id', transactionHandler.getOneTransaction);
  this.server.del('/api/v1/transaction/:id', transactionHandler.deleteOneTransaction);
  this.server.patch('/api/v1/transaction/:id', transactionHandler.patchOneTransaction);
};

module.exports = AppServer;
